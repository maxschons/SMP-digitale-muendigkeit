# Liste potenzieller Speaker

## Vorlage

Name, Vorname
Arbeitsbezeichnung / Fach:
Institution: 
Grund für unser Interesse:
Mögliche Vorträge / Workshops:
E-Mail:
Tel:
Standort:
Website: 

## Potenzielle Speaker geordnet nach Wunsch sie dabei zu haben

Vertreter von Purism
Arbeitsbezeichnung / Fach: Open-Source Hardware
Institution: Purism
Grund für unser Interesse: Hardware Hersteller der die Privatsphäre des Nutzers in den Mittelpunkt stellt.
Mögliche Vorträge / Workshops: Vorführungen im Showroom / Interview vor Publikum / Vortrag über die Probleme mit aktueller Hardware
E-Mail: info@puri.sm
Tel: - 
Standort: Eigentlich USA, aber auch irgendwo in Westdeutschland
Website: puri.sm

Name: Mühlhoff, Rainer
Fach: Postdoc Philosophie
Institution: FU Berlin
Möglicher Vortrag: Digitale Entmündigung und „UserExperience Design“. Über Nudging, Tracking und Infantilisierung im Netz
E-Mail: web@rmuehlhoff.de
Website: https://rainermuehlhoff.de/

Name: Kees, Benjamin
Arbeitsbezeichnung: Informatiker und Psychologe, Systemadministrator, Vorstandsmitglied beim FIfF e.V.
Möglicher Vortrag: Automatisierte Videoüberwachung. Von Computern, die auf Menschen starren
Institution: FIfF e. V., Gesellschaft für Informatik e. V.
Website: https://www.algoropticon.de/

Name: Kent, Caroline
Institution: Tactical Technology Collective
E-Mail: events@tacticaltech.org
Standort: Berlin
Workshop: [Glass Room Experience](https://tacticaltech.org/projects/glass-room-experience/)

Name: Kurz, Constanze
Institution: netzpolitik.org, Gesellschaft für Informatik e. V.
Grund für unser Interesse: hat ein Buch zur [digitalen Mündigkeit](http://datenfresser.info/?page_id=62) geschrieben
Website: http://gewissensbits.gi.de/constanze-kurz/

Name: Dr. Hagendorff, Thilo
Institution: Universität Tübingen (Forum Privatheit)
Mögliche Vorträge: siehe [Vortragsliste](https://www.thilo-hagendorff.info/vortraege/)
E-Mail: mail@thilo-hagendorff.info
Website: https://www.thilo-hagendorff.info/

Name: Rieger, Frank
Institution: CCC

Name: Keber, Tobias
Institution: Institut für digitale Ethik
Grund für unser Interesse: hat schon Sommerakademie-Workshop geleitet
Mögliche Vorträge: siehe [Vortragsliste](https://www.digitale-ethik.de/forschung/vortraege/)
Website: https://www.digitale-ethik.de/institut/leitung/

Name: Fuchs, Christian
Grund für unser Interesse: hat Buch [„Social Media: A Critical Introduction“](https://www.rkm-journal.de/archives/17072) geschrieben
Website: http://fuchs.uti.at/

Name: Zuboff, Shoshana
Grund für unser Interesse: [schreibt](http://www.faz.net/aktuell/feuilleton/debatten/die-google-gefahr-zuboff-antwortet-doepfner-12916606.html?printPagedArticle=true), hält Vorträge zum Thema Überwachung und Demokratie
Website: http://shoshanazuboff.com/